/* This file is part of the Palabos library.
 *
 * Copyright (C) 2011-2015 FlowKit Sarl
 * Route d'Oron 2
 * 1010 Lausanne, Switzerland
 * E-mail contact: contact@flowkit.com
 *
 * The most recent release of Palabos can be downloaded at
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** \file
 * Flow in a lid-driven 3D cavity. Benchmark case
 **/

#include "palabos3D.h"
#include "palabos3D.hh" // include full template code
#include <iostream>

using namespace plb;
using namespace std;

typedef double T;
#define DESCRIPTOR descriptors::D3Q19Descriptor

template <typename T> T polynomialVelocity(plint iX, plint iZ, T dx, T uMax) {
    T h = (T)1;
    T x = (T)iX * dx - (T)1;
    T z = (T)iZ * dx - (T)1;

    T u = uMax * pow((T)1 - pow(x / h, 18), 2) * pow((T)1 - pow(z / h, 18), 2);

    return u;
}

/// A functional, used to initialize the velocity for the boundary conditions
template <typename T> class LidPolynomialVelocity {
  public:
    LidPolynomialVelocity(T dx_, T uMax_) : dx(dx_), uMax(uMax_) {
    }
    void operator()(plint iX, plint iY, plint iZ, Array<T, 3> &u) const {
        u[0] = polynomialVelocity<T>(iX, iZ, dx, uMax);
        u[1] = T();
        u[2] = T();
    }

  private:
    T dx, uMax;
};

/// A functional, used to initialize the velocity for the boundary conditions
template <typename T> class LidPolynomialDensityAndVelocity {
  public:
    LidPolynomialDensityAndVelocity(T dx_, T uMax_) : dx(dx_), uMax(uMax_) {
    }
    void operator()(
        plint iX, plint iY, plint iZ, T &rho, Array<T, 3> &u) const {
        rho  = (T)1;
        u[0] = polynomialVelocity<T>(iX, iZ, dx, uMax);
        u[1] = T();
        u[2] = T();
    }

  private:
    T dx, uMax;
};

void cavitySetup(MultiBlockLattice3D<T, DESCRIPTOR> &lattice,
    IncomprFlowParam<T> const &parameters,
    OnLatticeBoundaryCondition3D<T, DESCRIPTOR> &boundaryCondition) {
    const plint nx            = parameters.getNx();
    const plint ny            = parameters.getNy();
    const plint nz            = parameters.getNz();
    Box3D topLid              = Box3D(0, nx - 1, ny - 1, ny - 1, 0, nz - 1);
    Box3D everythingButTopLid = Box3D(0, nx - 1, 0, ny - 2, 0, nz - 1);

    // All walls implement a Dirichlet velocity condition.
    boundaryCondition.setVelocityConditionOnBlockBoundaries(
        lattice, lattice.getBoundingBox(), boundary::dirichlet);

    T u = std::sqrt((T)2) / (T)2 * parameters.getLatticeU();
    initializeAtEquilibrium(
        lattice, everythingButTopLid, (T)1., Array<T, 3>((T)0., (T)0., (T)0.));

    initializeAtEquilibrium(lattice, topLid,
        LidPolynomialDensityAndVelocity<T>(parameters.getDeltaX(), u));
    setBoundaryVelocity(
        lattice, topLid, LidPolynomialVelocity<T>(parameters.getDeltaX(), u));

    lattice.initialize();
}

std::unique_ptr<MultiScalarField3D<T>> computeGridDensity(
    MultiBlockLattice3D<T, DESCRIPTOR> &lattice, T Re, T uMax) {

    T Ma         = uMax * sqrt(DESCRIPTOR<T>::invCs2);
    T Kn         = Ma / Re;
    Box3D domain = lattice.getBoundingBox();

    const plint nx = domain.getNx();
    const plint ny = domain.getNy();
    const plint nz = domain.getNz();

    std::unique_ptr<MultiScalarField3D<T>> rValues =
        generateMultiScalarField<T>(lattice, domain);

    applyProcessingFunctional(
        new ComputeRefinementRvalueFunctional3D<T, DESCRIPTOR>(Kn),
        Box3D(0, nx - 1, 0, ny - 1, 0, nz - 1), lattice, *rValues);

    // here we rescaled the rValues between 0 and 1.

    T minVal = computeMin(*rValues);
    subtractInPlace(*rValues, minVal);
    T maxVal = computeMax(*rValues);
    divideInPlace(*rValues, maxVal);

    return rValues;
}

void saveRefinement(
    MultiScalarField3D<T> &rValues, std::string &filename, T dx, plint iter) {

    plb_ofstream outFile;
    outFile.open(filename.c_str(), ios::out);
    Box3D domain = rValues.getBoundingBox();

    const plint nx = domain.getNx();
    const plint ny = domain.getNy();
    const plint nz = domain.getNz();

    // File format:
    // x0 x1 y0 y1 z0 z1 (physical units)
    // dx
    // nx ny nz (lattice units)
    // ascii data (between 0 and 1)
    outFile << -1 << " ";
    outFile << 1 << " ";
    outFile << -1 << " ";
    outFile << 1 << " ";
    outFile << -1 << " ";
    outFile << 1 << " ";
    outFile << std::endl;

    outFile << dx << " ";

    outFile << std::endl;
    outFile << nx << " ";
    outFile << ny << " ";
    outFile << nz << " ";

    outFile << std::endl;
    outFile << setprecision(10) << rValues << endl;
    outFile.close();

    ParallelVtkImageOutput3D<T> vtkOut(
        createFileName("rValues", iter, 6), 1, dx);
    vtkOut.writeData<float>(rValues, "rValue");
}

template <class BlockLatticeT>
void writeVTK(
    BlockLatticeT &lattice, IncomprFlowParam<T> const &parameters, plint iter) {
    T dx = parameters.getDeltaX();
    T dt = parameters.getDeltaT();

    // VtkImageOutput3D<T> vtkOut(createFileName("vtk", iter, 6), dx);
    ParallelVtkImageOutput3D<T> vtkOut(createFileName("vtk", iter, 6), 3, dx);

    vtkOut.writeData<3, float>(*computeVelocity(lattice), "velocity", dx / dt);
    vtkOut.writeData<float>(
        *computeVelocityNorm(lattice), "velocityNorm", dx / dt);
    vtkOut.writeData<3, float>(
        *computeVorticity(*computeVelocity(lattice)), "vorticity", 1. / dt);
}

int main(int argc, char *argv[]) {

    plbInit(&argc, &argv);
    global::directories().setOutputDir("./tmp/");

    const T uMax = (T)5e-2; // uMax
    T Re         = (T)0;
    plint N      = -1;
    try {
        global::argv(1).read(N);
        global::argv(2).read(Re);
    } catch (...) {
        pcout << "Wrong parameters. The syntax is " << std::endl;
        pcout << argv[0] << " N Re" << std::endl;
        pcout << std::endl;
        exit(1);
    }

    IncomprFlowParam<T> parameters(uMax, // uMax
        Re,                              // Re
        N,                               // N
        2.,                              // lx
        2.,                              // ly
        2.                               // lz
    );

    MultiBlockLattice3D<T, DESCRIPTOR> lattice(parameters.getNx(),
        parameters.getNy(), parameters.getNz(),
        new CompleteRegularizedBGKdynamics<T, DESCRIPTOR>(
            parameters.getOmega()));

    OnLatticeBoundaryCondition3D<T, DESCRIPTOR> *bc =
        createInterpBoundaryCondition3D<T, DESCRIPTOR>();

    cavitySetup(lattice, parameters, *bc);

    MultiScalarField3D<T> sumGridDensity(lattice);
    plint numIter = 100000;
    for (plint iT = 0; iT < numIter; ++iT) {
        if (iT % 1000 == 0) {
            pcout << "Iteration = " << iT << std::endl;
            writeVTK(lattice, parameters, iT);
            std::string filename =
                createFileName("./tmp/refinement", iT, 6) + ".dat";
            T div = (iT == 0) ? 1 : (T)1 / (T)iT;
            saveRefinement(*multiply(sumGridDensity, div), filename,
                parameters.getDeltaX(), iT);
        }
        lattice.collideAndStream();
        std::unique_ptr<MultiScalarField3D<T>> rValues =
            computeGridDensity(lattice, Re, uMax);
        addInPlace(sumGridDensity, *rValues);
    }

    delete bc;
}
