# Grid density generation

The aim of this program is the generation of a grid density field with values between
0 and 1 that are then used by the mesh generation in grid refinement simulations.

The higher the grid denisity the finer the grid will be, the lower the grid density
the coarser the grid.

In this example we are going to construct a grid density for the flow past a sphere using spherical domains.

## Compilation

To compile this project

```bash
mkdir build

cd build
cmake .. && make
cd ..
```

## Execution

```bash
./generateGridDensityFromBoxes simpleSphere.xml
```

## The sphere

The geometry of the sphere can be read from `sphere.stl`. It represents a diameter one
sphere in [STL](https://bit.ly/2YeMxtF) file format which center is at position `(0, 0, 0)`
and has a diameter of one.
One can read it with the [Paraview](https://www.paraview.org/) library.

## Parameters

The parameters for the grid density generation are read from the `simpleSphere.xml` file. There are four different
kind of parameters:

1. The `fullDomain` which is the bounding box in physical units of the simulation domain.
2. The different levels which contain spherical domains. Each level represent a different grid density
value and can contain many rectangular domains. The grid density of each domain is automatically
adjusted between 0 and 1.
3. The resolution `N` which is the number of grid points in the `x` direction.
4. And the number of smoothing operations `numSmooth` which is larger or equal to zero
and helps make transitions between grid density values smoother. The smoothing operation is nothing else than
a Laplacian filter.

## Output

The are two output files. One `gridDensity.vti` file that can be visualized with the Paraview
softwarre and a `gridDensity.dat` file that is an ASCII file containing the grid density
at each mesh point, as well as the resolutoin and full domain size.

# Exercise

## Spherical domains

In this exercise you will adapt the code to generate sperical grid density domains
and use the config file `simpleSphere.xml` to generate three domains (one at each level).

You will find the places where the code should be written containig the comments `// Exercise`.
