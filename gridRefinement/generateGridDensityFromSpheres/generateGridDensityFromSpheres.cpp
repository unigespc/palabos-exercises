/* This file is part of the Palabos library.
 *
 * The Palabos softare is developed since 2011 by FlowKit-Numeca Group Sarl
 * (Switzerland) and the University of Geneva (Switzerland), which jointly
 * own the IP rights for most of the code base. Since October 2019, the
 * Palabos project is maintained by the University of Geneva and accepts
 * source code contributions from the community.
 *
 * Contact:
 * Jonas Latt
 * Computer Science Department
 * University of Geneva
 * 7 Route de Drize
 * 1227 Carouge, Switzerland
 * jonas.latt@unige.ch
 *
 * The most recent release of Palabos can be downloaded at
 * <https://palabos.unige.ch/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>

#include "palabos3D.h"
#include "palabos3D.hh"

using namespace plb;

typedef double T;

#define SM_DESCRIPTOR descriptors::AdvectionDiffusionD3Q7Descriptor

std::string outDir("./tmp/");

template <typename T>
struct Sphere {
    Sphere(Array<T, 3> const &center_, T radius_) : center(center_), radius(radius_) {}

    /// Initialize the data from a 4-element array.
    Sphere(Array<T, 4> const &array) {
        center[0] = array[0];
        center[1] = array[1];
        center[2] = array[2];
        radius = array[3];
    }

    Sphere(T x, T y, T z, T radius_) {
        center = Array<T, 3>(x, y, z);
        radius = radius_;
    }

    void translate(Array<T, 3> const &vector) { center += vector; }

    void scale(T alpha) { radius *= alpha; }

    /// Copy the data into a 4-element array.
    Array<T, 4> to_plbArray() const {
        Array<T, 4> array;
        array[0] = cx();
        array[1] = cy();
        array[2] = cz();
        array[3] = r();

        return array;
    }

    T cx() const { return center[0]; }
    T cy() const { return center[1]; }
    T cz() const { return center[2]; }
    T r() const { return radius; }

    std::string toStr() const {
        return util::val2str(center[0])+", "+util::val2str(center[1])
            +", "+util::val2str(center[2])+"; "+util::val2str(radius);
    }

    Array<T, 3> center;
    T radius;
};

bool contained(plint x, plint y, plint z, Sphere<plint> const &s) {
    plint iX = x - s.cx();
    plint iY = y - s.cy();
    plint iZ = z - s.cz();

    return (iX * iX + iY * iY + iZ * iZ <= s.r() * s.r());
}

struct Parameters {
    // A Sphere is the equivalent of a Box3D but with numbers
    Cuboid<T> fullDomain;  // Domain of definition for the grid function.

    std::vector<std::vector<Sphere<T>>> spheres;  // All spheres for all levels.
    plint nx, ny, nz;
    plint numSmooth;  // Number of times to smooth the grid density function
                      // before output.
    T dx;             // Resolution determined by the length of the fulldomain divided by nx.
    Array<T, 3> physicalLocation;                     // x0(), y0(), z0() of the fullDomain sphere
    std::vector<std::vector<Sphere<plint>>> domains;  // spheres in lb units
};

// ========================================================================= //
// Exercise:
// Add a function to rescale the sphere in LB units
// Solution:
Sphere<plint> toLB(Sphere<T> const &physVal, T dx, Array<T, 3> const &location) {
    // Solution:
    Sphere<plint> sph = Sphere<plint>(util::roundToInt((physVal.cx() - location[0]) / dx),
                                      util::roundToInt((physVal.cy() - location[1]) / dx),
                                      util::roundToInt((physVal.cz() - location[2]) / dx),
                                      util::roundToInt(physVal.r() / dx));

    return sph;
}
// ========================================================================= //

// reading xml file
void setupParameters(std::string xmlInputFileName, Parameters &param) {
    XMLreader document(xmlInputFileName);

    Array<T, 6> fullDomain;
    document["fullDomain"].read<T, 6>(fullDomain);
    param.fullDomain.from_plbArray(fullDomain);
    param.physicalLocation =
        Array<T, 3>(param.fullDomain.x0(), param.fullDomain.y0(), param.fullDomain.z0());

    XMLreaderProxy levels = document["level"];
    for (; levels.isValid(); levels = levels.iterId()) {
        // ========================================================================= //
        // Exercise: 
        // Inspired by the generateGridDensityFromBoxes code
        // read the spheres at each level.

        // Solution: 
        std::vector<Sphere<T>> spheresAtLevel;
        XMLreaderProxy spheres = levels["domain"];
        for (; spheres.isValid(); spheres = spheres.iterId()) {
            Array<T, 4> nextSphereArray;
            spheres.read<T, 4>(nextSphereArray);
            Sphere<T> nextSphere = Sphere<T>(nextSphereArray);
            spheresAtLevel.push_back(nextSphere);
        }
        param.spheres.push_back(spheresAtLevel);
        // ========================================================================= //
    }

    document["N"].read(param.nx);
    PLB_ASSERT(param.nx >= 2);

    T lx = param.fullDomain.x1() - param.fullDomain.x0();
    T ly = param.fullDomain.y1() - param.fullDomain.y0();
    T lz = param.fullDomain.z1() - param.fullDomain.z0();
    param.dx = lx / (param.nx - 1);
    param.ny = util::roundToInt(ly / param.dx) + 1;
    param.nz = util::roundToInt(lz / param.dx) + 1;

    document["numSmooth"].read(param.numSmooth);
    PLB_ASSERT(param.numSmooth >= 0);

    for (plint iLevel = 0; iLevel < (plint)param.spheres.size(); iLevel++) {
        // ========================================================================= //
        // Exercise: 
        // Inspired by the generateGridDensityFromBoxes code
        // transform the spheres from pyhsical coordinates
        // to lattice coordinates. You will need to implement the
        // function toLb function which signature you can find
        // above in the code.

        // Solution: 
        std::vector<Sphere<plint>> lbSpheresAtLevel;
        std::vector<Sphere<T>> const &spheresAtLevel = param.spheres[iLevel];
        for (plint iSphere = 0; iSphere < (plint)spheresAtLevel.size(); iSphere++) {
            Sphere<plint> sphere = toLB(spheresAtLevel[iSphere], param.dx, param.physicalLocation);
            lbSpheresAtLevel.push_back(sphere);
        }
        param.domains.push_back(lbSpheresAtLevel);
        // ========================================================================= //
    }
}

void printParameters(Parameters const &param) {
    pcout << "Spheres:" << std::endl;
    for (plint iLevel = 0; iLevel < (plint)param.spheres.size(); iLevel++) {
        pcout << "    iLevel = " << iLevel + 1 << std::endl;
        std::vector<Sphere<T>> const &spheresAtLevel = param.spheres[iLevel];
        for (plint iSphere = 0; iSphere < (plint)spheresAtLevel.size(); iSphere++) {
            Sphere<T> const &s = spheresAtLevel[iSphere];
            pcout << "    sphere[" << iSphere << "] = [" << s.toStr() << "] "
                  << std::endl;
        }
    }
    pcout << "numSmooth = " << param.numSmooth << std::endl;
    pcout << "dx = " << param.dx << std::endl;
    pcout << "physicalLocation = (" << param.physicalLocation[0] << ", "
          << param.physicalLocation[1] << ", " << param.physicalLocation[2] << ")" << std::endl;
    pcout << "Domains (LU):" << std::endl;
    for (plint iLevel = 0; iLevel < (plint)param.domains.size(); iLevel++) {
        pcout << "    iLevel = " << iLevel + 1 << std::endl;
        std::vector<Sphere<plint>> const &lbSpheresAtLevel = param.domains[iLevel];
        for (plint iSphere = 0; iSphere < (plint)lbSpheresAtLevel.size(); iSphere++) {
            Sphere<plint> const &s = lbSpheresAtLevel[iSphere];
            pcout << "    sphere[" << iSphere << "] = [" << s.toStr() << "] " << std::endl;
        }
    }
}

void writeGridDensity(Parameters const &param, MultiScalarField3D<T> &gridDensity) {
    std::string fname = outDir + "gridDensity";
    VtkImageOutput3D<T> vtkOut(fname, param.dx, param.physicalLocation);
    vtkOut.writeData<float>(gridDensity, "gridDensity", (T)1);

    std::string outputFileName = outDir + "gridDensity.dat";
    plb_ofstream outputFile(outputFileName.c_str());
    if (!outputFile.is_open()) {
        pcout << "Error: could not open output file " << outputFileName << std::endl;
        exit(1);
    }

    outputFile.getOriginalStream().precision(std::numeric_limits<T>::digits10);
    outputFile << param.fullDomain.x0() << " " << param.fullDomain.x1() << " "
               << param.fullDomain.y0() << " " << param.fullDomain.y1() << " "
               << param.fullDomain.z0() << " " << param.fullDomain.z1() << std::endl;
    outputFile << param.dx << std::endl;
    outputFile << param.nx << " " << param.ny << " " << param.nz << std::endl;
    outputFile << gridDensity;

    outputFile.close();
}

template <typename T>
class ComputeGridDensityFromSpheres3D : public BoxProcessingFunctional3D_S<T> {
public:
    ComputeGridDensityFromSpheres3D(std::vector<std::vector<Sphere<plint>>> const &spheres_)
        : spheres(spheres_) {}

    virtual void process(Box3D domain, ScalarField3D<T> &gridDensity) {
        // ========================================================================= //
        // Exercise: 
        // Inspired by the generateGridDensityFromBoxes code fill the
        // gridDensity ScalaField3D with the proper grid density values
        // based on the spheres variable.

        // Solution:
        plint numLevels = spheres.size() + 1;
        T df = (T)1 / (T)(numLevels - 1);

        Dot3D absOfs = gridDensity.getLocation();
        for (plint iX = domain.x0; iX <= domain.x1; iX++) {
            plint x = iX + absOfs.x;
            for (plint iY = domain.y0; iY <= domain.y1; iY++) {
                plint y = iY + absOfs.y;
                for (plint iZ = domain.z0; iZ <= domain.z1; iZ++) {
                    plint z = iZ + absOfs.z;

                    for (plint iLevel = 0; iLevel < numLevels - 1;
                         iLevel++) {  // Level-0 is not included.
                        T f = (T)(iLevel + 1) * df;
                        for (plint iSphere = 0; iSphere < (plint)spheres[iLevel].size();
                             iSphere++) {
                            if (contained(x, y, z, spheres[iLevel][iSphere])) {
                                gridDensity.get(iX, iY, iZ) = f;
                                break;
                            }
                        }
                    }
                }
            }
        }
        // ========================================================================= //
    }

    virtual ComputeGridDensityFromSpheres3D<T> *clone() const {
        return new ComputeGridDensityFromSpheres3D<T>(*this);
    }

    virtual void getTypeOfModification(std::vector<modif::ModifT> &modified) const {
        modified[0] = modif::staticVariables;
    }

    virtual BlockDomain::DomainT appliesTo() const { return BlockDomain::bulkAndEnvelope; }

private:
    std::vector<std::vector<Sphere<plint>>> const &spheres;
};

// Sets the values to 0 when they are smaller than 0.
// Sets the values to 1 when they are larger than 1.
template <typename T>
class CeilAndFloorGridDensity3D : public BoxProcessingFunctional3D_S<T> {
public:
    virtual void process(Box3D domain, ScalarField3D<T> &gridDensity) {
        for (plint iX = domain.x0; iX <= domain.x1; ++iX) {
            for (plint iY = domain.y0; iY <= domain.y1; ++iY) {
                for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ) {
                    T &gd = gridDensity.get(iX, iY, iZ);
                    if (gd < (T)0) {
                        gd = (T)0;
                    } else if (gd > (T)1) {
                        gd = (T)1;
                    }
                }
            }
        }
    }

    virtual CeilAndFloorGridDensity3D<T> *clone() const {
        return new CeilAndFloorGridDensity3D<T>(*this);
    }

    virtual void getTypeOfModification(std::vector<modif::ModifT> &modified) const {
        modified[0] = modif::staticVariables;
    }

    virtual BlockDomain::DomainT appliesTo() const { return BlockDomain::bulkAndEnvelope; }
};

int main(int argc, char *argv[]) {
    plbInit(&argc, &argv);

    std::cout.precision(6);
    std::scientific(std::cout);

    // Command-line arguments
    if (argc != 2) {
        pcout << "Usage: " << argv[0] << " xml-input-file-name " << std::endl;
        pcout << "Example: ./generateGridDensityFromBoxes simpleSphere.xml" << std::endl;
        exit(1);
    }

    std::string xmlInputFileName;
    xmlInputFileName = std::string(argv[1]);

    // Set the simulation parameters.

    Parameters param;

    setupParameters(xmlInputFileName, param);
    printParameters(param);

    MultiScalarField3D<T> *gridDensity =
        new MultiScalarField3D<T>(param.nx, param.ny, param.nz, (T)0);
    // ======================================================= //
    // Exercise: 
    // Apply you newly implemented processing functional to the grid
    // density scalar field.
    
    // Solution:
    applyProcessingFunctional(new ComputeGridDensityFromSpheres3D<T>(param.domains),
                              gridDensity->getBoundingBox(), *gridDensity);
    // ======================================================= //

    // smoothening of the solution numSmooth times
    for (plint i = 0; i < param.numSmooth; i++) {
        MultiScalarField3D<T> *smoothedGridDensity =
            lbmSmoothen<T, SM_DESCRIPTOR>(*gridDensity).release();
        std::swap(gridDensity, smoothedGridDensity);
        delete smoothedGridDensity;
    }

    applyProcessingFunctional(new CeilAndFloorGridDensity3D<T>(), gridDensity->getBoundingBox(),
                              *gridDensity);

    writeGridDensity(param, *gridDensity);

    delete gridDensity;

    return EXIT_SUCCESS;
}
