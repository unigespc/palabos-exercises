/* This file is part of the Palabos library.
 *
 * The Palabos softare is developed since 2011 by FlowKit-Numeca Group Sarl
 * (Switzerland) and the University of Geneva (Switzerland), which jointly
 * own the IP rights for most of the code base. Since October 2019, the
 * Palabos project is maintained by the University of Geneva and accepts
 * source code contributions from the community.
 *
 * Contact:
 * Jonas Latt
 * Computer Science Department
 * University of Geneva
 * 7 Route de Drize
 * 1227 Carouge, Switzerland
 * jonas.latt@unige.ch
 *
 * The most recent release of Palabos can be downloaded at
 * <https://palabos.unige.ch/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>

#include "palabos3D.h"
#include "palabos3D.hh"

using namespace plb;

typedef double T;

#define SM_DESCRIPTOR descriptors::AdvectionDiffusionD3Q7Descriptor

std::string outDir("./tmp/");

struct Parameters {
    // A Cuboid is the equivalent of a Box3D but with numbers
    Cuboid<T> fullDomain;  // Domain of definition for the grid function.

    std::vector<std::vector<Cuboid<T>>> cuboids;  // All cuboids for all levels.
    plint nx, ny, nz;
    plint numSmooth;  // Number of times to smooth the grid density function
                      // before output.
    T dx;             // Resolution determined by the length of the fulldomain divided by nx.
    Array<T, 3> physicalLocation;             // x0(), y0(), z0() of the fullDomain cuboid
    std::vector<std::vector<Box3D>> domains;  // cuboids in lb units
};

Box3D toLB(Cuboid<T> const &physVal, T dx, Array<T, 3> const &location) {
    Box3D domain = Box3D(util::roundToInt((physVal.x0() - location[0]) / dx),
                         util::roundToInt((physVal.x1() - location[0]) / dx),
                         util::roundToInt((physVal.y0() - location[1]) / dx),
                         util::roundToInt((physVal.y1() - location[1]) / dx),
                         util::roundToInt((physVal.z0() - location[2]) / dx),
                         util::roundToInt((physVal.z1() - location[2]) / dx));

    return domain;
}

// reading xml file
void setupParameters(std::string xmlInputFileName, Parameters &param) {
    XMLreader document(xmlInputFileName);

    Array<T, 6> fullDomain;
    document["fullDomain"].read<T, 6>(fullDomain);
    param.fullDomain.from_plbArray(fullDomain);
    param.physicalLocation =
        Array<T, 3>(param.fullDomain.x0(), param.fullDomain.y0(), param.fullDomain.z0());

    XMLreaderProxy levels = document["level"];
    for (; levels.isValid(); levels = levels.iterId()) {
        std::vector<Cuboid<T>> cuboidsAtLevel;
        XMLreaderProxy cuboids = levels["domain"];
        for (; cuboids.isValid(); cuboids = cuboids.iterId()) {
            Array<T, 6> nextCuboidArray;
            cuboids.read<T, 6>(nextCuboidArray);
            Cuboid<T> nextCuboid;
            nextCuboid.from_plbArray(nextCuboidArray);
            cuboidsAtLevel.push_back(nextCuboid);
        }
        param.cuboids.push_back(cuboidsAtLevel);
    }

    document["N"].read(param.nx);
    PLB_ASSERT(param.nx >= 2);

    T lx = param.fullDomain.x1() - param.fullDomain.x0();
    T ly = param.fullDomain.y1() - param.fullDomain.y0();
    T lz = param.fullDomain.z1() - param.fullDomain.z0();
    param.dx = lx / (param.nx - 1);
    param.ny = util::roundToInt(ly / param.dx) + 1;
    param.nz = util::roundToInt(lz / param.dx) + 1;

    document["numSmooth"].read(param.numSmooth);
    PLB_ASSERT(param.numSmooth >= 0);

    for (plint iLevel = 0; iLevel < (plint)param.cuboids.size(); iLevel++) {
        std::vector<Box3D> domainsAtLevel;
        std::vector<Cuboid<T>> const &cuboidsAtLevel = param.cuboids[iLevel];
        for (plint iCuboid = 0; iCuboid < (plint)cuboidsAtLevel.size(); iCuboid++) {
            Box3D domain = toLB(cuboidsAtLevel[iCuboid], param.dx, param.physicalLocation);
            domainsAtLevel.push_back(domain);
        }
        param.domains.push_back(domainsAtLevel);
    }
}

void printParameters(Parameters const &param) {
    pcout << "Domains:" << std::endl;
    for (plint iLevel = 0; iLevel < (plint)param.cuboids.size(); iLevel++) {
        pcout << "    iLevel = " << iLevel + 1 << std::endl;
        std::vector<Cuboid<T>> const &cuboidsAtLevel = param.cuboids[iLevel];
        for (plint iCuboid = 0; iCuboid < (plint)cuboidsAtLevel.size(); iCuboid++) {
            Cuboid<T> const &d = cuboidsAtLevel[iCuboid];
            pcout << "    domain[" << iCuboid << "] = [" << d.x0() << ", " << d.x1() << "; "
                  << d.y0() << ", " << d.y1() << "; " << d.z0() << ", " << d.z1() << "] "
                  << std::endl;
        }
    }
    pcout << "numSmooth = " << param.numSmooth << std::endl;
    pcout << "dx = " << param.dx << std::endl;
    pcout << "physicalLocation = (" << param.physicalLocation[0] << ", "
          << param.physicalLocation[1] << ", " << param.physicalLocation[2] << ")" << std::endl;
    pcout << "Domains (LU):" << std::endl;
    for (plint iLevel = 0; iLevel < (plint)param.domains.size(); iLevel++) {
        pcout << "    iLevel = " << iLevel + 1 << std::endl;
        std::vector<Box3D> const &domainsAtLevel = param.domains[iLevel];
        for (plint iCuboid = 0; iCuboid < (plint)domainsAtLevel.size(); iCuboid++) {
            Box3D const &d = domainsAtLevel[iCuboid];
            pcout << "    domain[" << iCuboid << "] = [" << d.toStr() << "] " << std::endl;
        }
    }
}

void writeGridDensity(Parameters const &param, MultiScalarField3D<T> &gridDensity) {
    std::string fname = outDir + "gridDensity";
    // VtkImageOutput3D<T> vtkOut(fname, param.dx, param.physicalLocation);
    VtkAsciiImageOutput3D<T> vtkOut(fname, "gridDensity", param.dx, param.physicalLocation);
    vtkOut.writeData<float>(gridDensity, "gridDensity", (T)1);

    std::string outputFileName = outDir + "gridDensity.dat";
    plb_ofstream outputFile(outputFileName.c_str());
    if (!outputFile.is_open()) {
        pcout << "Error: could not open output file " << outputFileName << std::endl;
        exit(1);
    }

    outputFile.getOriginalStream().precision(std::numeric_limits<T>::digits10);
    outputFile << param.fullDomain.x0() << " " << param.fullDomain.x1() << " "
               << param.fullDomain.y0() << " " << param.fullDomain.y1() << " "
               << param.fullDomain.z0() << " " << param.fullDomain.z1() << std::endl;
    outputFile << param.dx << std::endl;
    outputFile << param.nx << " " << param.ny << " " << param.nz << std::endl;
    outputFile << gridDensity;

    outputFile.close();
}

template <typename T>
class ComputeGridDensityFromBoxes3D : public BoxProcessingFunctional3D_S<T> {
public:
    ComputeGridDensityFromBoxes3D(std::vector<std::vector<Box3D>> const &domains_)
        : domains(domains_) {}

    virtual void process(Box3D domain, ScalarField3D<T> &gridDensity) {
        plint numLevels = domains.size() + 1;
        T df = (T)1 / (T)(numLevels - 1);

        Dot3D absOfs = gridDensity.getLocation();
        for (plint iX = domain.x0; iX <= domain.x1; iX++) {
            plint x = iX + absOfs.x;
            for (plint iY = domain.y0; iY <= domain.y1; iY++) {
                plint y = iY + absOfs.y;
                for (plint iZ = domain.z0; iZ <= domain.z1; iZ++) {
                    plint z = iZ + absOfs.z;

                    for (plint iLevel = 0; iLevel < numLevels - 1;
                         iLevel++) {  // Level-0 is not included.
                        T f = (T)(iLevel + 1) * df;
                        for (plint iCuboid = 0; iCuboid < (plint)domains[iLevel].size();
                             iCuboid++) {
                            if (contained(x, y, z, domains[iLevel][iCuboid])) {
                                gridDensity.get(iX, iY, iZ) = f;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    virtual ComputeGridDensityFromBoxes3D<T> *clone() const {
        return new ComputeGridDensityFromBoxes3D<T>(*this);
    }

    virtual void getTypeOfModification(std::vector<modif::ModifT> &modified) const {
        modified[0] = modif::staticVariables;
    }

    virtual BlockDomain::DomainT appliesTo() const { return BlockDomain::bulkAndEnvelope; }

private:
    std::vector<std::vector<Box3D>> const &domains;
};

// Sets the values to 0 when they are smaller than 0.
// Sets the values to 1 when they are larger than 1.
template <typename T>
class CeilAndFloorGridDensity3D : public BoxProcessingFunctional3D_S<T> {
public:
    virtual void process(Box3D domain, ScalarField3D<T> &gridDensity) {
        for (plint iX = domain.x0; iX <= domain.x1; ++iX) {
            for (plint iY = domain.y0; iY <= domain.y1; ++iY) {
                for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ) {
                    T &gd = gridDensity.get(iX, iY, iZ);
                    if (gd < (T)0) {
                        gd = (T)0;
                    } else if (gd > (T)1) {
                        gd = (T)1;
                    }
                }
            }
        }
    }

    virtual CeilAndFloorGridDensity3D<T> *clone() const {
        return new CeilAndFloorGridDensity3D<T>(*this);
    }

    virtual void getTypeOfModification(std::vector<modif::ModifT> &modified) const {
        modified[0] = modif::staticVariables;
    }

    virtual BlockDomain::DomainT appliesTo() const { return BlockDomain::bulkAndEnvelope; }
};

int main(int argc, char *argv[]) {
    plbInit(&argc, &argv);

    std::cout.precision(6);
    std::scientific(std::cout);

    // Command-line arguments
    if (argc != 2) {
        pcout << "Usage: " << argv[0] << " xml-input-file-name " << std::endl;
        pcout << "Example: ./generateGridDensityFromBoxes simpleSphere.xml" << std::endl;
        exit(1);
    }

    std::string xmlInputFileName;
    xmlInputFileName = std::string(argv[1]);

    // Set the simulation parameters.

    Parameters param;

    setupParameters(xmlInputFileName, param);
    printParameters(param);

    MultiScalarField3D<T> *gridDensity =
        new MultiScalarField3D<T>(param.nx, param.ny, param.nz, (T)0);
    applyProcessingFunctional(new ComputeGridDensityFromBoxes3D<T>(param.domains),
                              gridDensity->getBoundingBox(), *gridDensity);

    // smoothening of the solution numSmooth times
    for (plint i = 0; i < param.numSmooth; i++) {
        MultiScalarField3D<T> *smoothedGridDensity =
            lbmSmoothen<T, SM_DESCRIPTOR>(*gridDensity).release();
        std::swap(gridDensity, smoothedGridDensity);
        delete smoothedGridDensity;
    }

    applyProcessingFunctional(new CeilAndFloorGridDensity3D<T>(), gridDensity->getBoundingBox(),
                              *gridDensity);

    writeGridDensity(param, *gridDensity);

    delete gridDensity;

    return EXIT_SUCCESS;
}
