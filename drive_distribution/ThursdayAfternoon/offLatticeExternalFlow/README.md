# Grid generation

The multi-level grid generation in Palabos has an octree structure
that is built from a grid denisity file: the highest the grid density, the finer the mesh. 

There are three different parameters controlling the mesh generation:

1. The maximal octree level `maxOctreeLevel` which represents the maximal number of octree subdisions to generate the mesh.
2. The maximal number of levels `numLevels` which is the maximal number of octree levels that are kept for the mesh. There are consistency requirements of havin at least a block when transition between mesh resolutions and therefore the may be less levels than `numLevels` in the final mesh.
3. The size of a block `nBlock` which is the number of grid points in each block. Each octree block at each level has the same amount of mesh points.

## Compilation

To compile this project

```bash
mkdir build

cd build
cmake .. && make
cd ..
```

## Execution

```bash
./offLatticeExternalFlow config.xml
```

## Important note

In the `config.xml` file the is a `dryRun` parameter. This parameter is `true` when one only wants to inspect the octree grid
structure and does not want to actually run the simulation. It is `false` when one wants to generate the octree grid structure
and run the simulation.

# Playing with grid densities and mesh parameters

As a first exercise we want to get a grasp of what meshes
one might obtain by varying the grid density and the mesh parameters.

1. Generate at least two grid densities (see previous exercises) and run dry runs with the `config.xml` config file.
2. Inspect the generated octree grid structure by looking at the `tmp/octree_after_calibration.stl` file that can visualized with Paraview (using the `Slice` filter).
3. Try adding more octree levels by changing `numLevels` and `maxOctreeLevel` (in `config.xml`) and vizualize the result.

# Adding Reynolds stresses

In order to analyze turbulent flows it is common to compute the Reynolds stresses.

In this exercise you can inspire yourself from the computation of the average velocity
to add the computation of Reynolds stresses and write them on the disk. Do not forget to also add them in the checkpointing also.

In the sample code provided you will need to add pieces of code at the different locations
annotated with the comment `Exercise REYNOLDS`. There are places where pieces of code
are already written to simplify your work. Those are annotated `nothing to do`.

## Useful Palabos functionalities

In order to compute the Reynolds stresses, a very useful data processor is `UpdateDevTensorTransientStatistics3D`
that can be found in the file `palabos/src/dataProcessors/dataAnalysisFunctional3D.h/.hh`.

# Adding probes

Another very common output one needs from simulations is to have macroscopic quantities on precise locations.
In Palabos these quantities are obtained by using probes.

In this exercise you will add velocity and vorticity probes on different locations, defined in the `config.xml` file and write them to the disk.

You will find the places where the code should be written containig the comments `// Exercise PROBES`.
There are places where pieces of code
are already written to simplify your work. Those are annotated `nothing to do`.

# Changing the collision model: How to

This exercise is a `bonus` exercise (only perform it once you have finished the rest) and not much information is given.

To change the collision model, two different things must be performed.

1. Replace the `ConsistentSmagorinskyCompleteRegularizedBGKdynamics` by the dynamics one wants to use.
2. Create a new `Recaler` if needed. Currently the rescaler used is `ConvectiveNoForceRescaler` which can be found in `palabos/src/gridRefinement/rescaleEngine.h/.hh`.
3. Inspired by the rescalers one can find there, one can implement for example the rescaler for MRT models.

