# First Palabos example

## Introduction
The goal of this exercise is to familiarize yourself with basic Palabos programs. You will simulate a flow in a lid-driven 2D cavity.


#### Tasks list
1. Complete the code wherever a comment invites you to do so.
2. Run it and observe the problem. Fix it.

## Guide
#### Install cmake and paraview
To compile this example you need Cmake installed in your system. To check the installation
type on your terminal: `cmake --version`.
If it returns an error, you need to proceed with the installation. Try with the following command:
* archlinux based (manjaro): `sudo pacman -S cmake`
* debian based (ubuntu): `sudo apt install cmake`
* redhat based (opensuse,fedora, centos): `yum install cmake`

for other systems, please refer to the [official installation instructions](https://cmake.org/install/).

You will also need to have installed `paraview` in your machine to read the output files and  
(optionally) `gifsicle` to merge `.gif` files created by Palabos. Test if they are already installed with installed:
`gifsicle --version` and `paraview --version`. If this is not the case, try to install them
using your system specific command:
* archlinux based: `sudo pacman -S gifsicle` and `sudo pacman -S paraview`
* debian based (ubuntu):
* redhat based (opensuse,fedora, centos):

#### Program compilation
Once your program is corrected, you can compile it.

If the Palabos root folder is in your path, the provided CMakeLists.txt
should already be operative. In the other case, you need either to add
Palabos to your path or in alternative modify the CMakeLists.txt explicitly
setting the path to palabos:
```
# comment the following line:
file(TO_CMAKE_PATH $ENV{PALABOS_ROOT} PALABOS_ROOT)
# uncomment the following line:
#set(PALABOS_ROOT path/to/palabos)
```

Open a terminal in the current directory and type the following commands
to compile the code with cmake:
```
cd build
cmake .. && make -j 2
cd ..
```
At this point you should have an executable in the current directory  
named `cavity_2d_incomplete_with_bug`. Try to run it in parallel
```
mpirun -np 2 cavity_2d_incomplete_with_bug
```
in the case of a linux-based system.

