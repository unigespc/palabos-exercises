# Curved boundary conditions

## Introduction
The goal of this exercise is to familiarize with off-lattice boundary conditions in Palabos.
The exercise will cover the following topics
* Compile an example with cmake;
* Use Palabos gif writer and vti writer;
* Use paraview to analize the results;
* The consistent setup of _lattice_ and _dimensionless_ units;
* How to use the voxelizer;
* Generate TriangleSets;
* Use sponge zones;
* Use off-lattice boundary conditions implemented in Palabos;
* Compute the drag coefficient.

Which boundary condition can be implemented:
* Guo boundary condition (GZS) [1]
* Bouzidi boundary condition (BFL) [2]
* Filippova-Hanel boundary condition (FH) [3]
* Mei-Luo-Shyy (MLS) [4]

What it does not cover:
* Moving objects
* Refill algorithms
* Immersed Boundary method (IBM)

Suggested text books: 
* "The Lattice Boltzmann Method" Kruger et al. (2016) [5]
* "The Lattice Boltzmann equation" Succi (2018) [6]


#### Tasks list
1. Install cmake, gifsicle, paraview
2. Test the compilation with CMake
3. Run the 2d bounce-back simulation
    * Try to set different relaxation time $\tau$ keeping the 
    dimensionless parameters unchanged;
    * Try to set LBM max velocity in lattice units $u_lb$ keeping the
           dimensionless parameters unchanged;
    * (optional) Modify the external boundary conditions to have a free-slip
      configuration instead of a Poiseuille channel flow;
4. Try to compile the 3d version and then run it after choosing the simulation parameters;
5. Compute the drag coefficient and write it on a file using pcout;
6. (optional) Use the TRT dynamics for low Reynolds and Smagorinsky for the turbulent case.
7. Test different off-lattice boundary conditions
    * How does Mei-Luo-Shyy and Filippova-Haenel (FH) compare in terms of stability? Find parameters for which FH is stable.
    * Compute the drag for each method and save them on different files;
    * Implement a sponge zone to dump pressure waves.
    * Using Bouzidi or Guo, add a second sphere  behind the first  one;
    * Transform the two sphere into ellipsoids.

## Guide
#### Install cmake and paraview
To compile this example you need Cmake installed in your system. To check the installation
type on your terminal: `cmake --version`.
If it returns an error, you need to proceed with the installation. Try with the following command:
* archlinux based (manjaro): `sudo pacman -S cmake`
* debian based (ubuntu): `sudo apt install cmake`
* redhat based (opensuse,fedora, centos): `yum install cmake`

for other systems, please refer to the [official installation instructions](https://cmake.org/install/).

You will also need to have installed `paraview` in your machine to read the output files and  
(optionally) `gifsicle` to merge `.gif` files created by Palabos. Test if they are already installed with installed:
`gifsicle --version` and `paraview --version`. If this is not the case, try to install them
using your system specific command:
* archlinux based: `sudo pacman -S gifsicle` and `sudo pacman -S paraview`
* debian based (ubuntu):
* redhat based (opensuse,fedora, centos):

#### Test the compilation
If Palabos is on the directory of the exercises, the provided CMakeLists.txt
should already be operative. In all the other cases, you can check the following lines  
of the CMakeLists.txt to modify the palabos location:
```
# # # ADD PALABOS PATH:
# # CASE 1: PALABOS_ROOT is on your path
# file(TO_CMAKE_PATH $ENV{PALABOS_ROOT} PALABOS_ROOT)
# # CASE 2: specify an ABSOLUTE DIRECTORY PATH
#set(PALABOS_ROOT path/to/palabos)
# # CASE 3: specify a relative path to the build folder
set(PALABOS_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/../palabos)
```

Open a terminal in the current directory and type the following commands
to compile the code with cmake:
```
cd build
cmake .. && make -j 2
cd ..
```
At this point you should have an executable in the current directory  
named `cbc_example`. Try to run it in parallel
```
mpirun -np 2 cbc_example
```
in the case of a linux-based system.

### Warm-up: 2D simulation with simple bounce-back
Open the main file `cbc_example_2d.cpp`. Go to the following section:
```
    // Define simulation parameters: we use two ad-hoc units helper check simulation_parameters.h
    // for the documentation.
    const Real re = 250; // NB: Obstacle reynolds!
    sp::Numerics<Real, Int> lu;
    sp::NonDimensional<Real> dimless;
    dimless.init_re_lxlylz(re,24,4,4).print_parameters();
    IncomprFlowParam<Real> parameters
            = lu.init_lreflu_nodim(25, &dimless, 0.02, false)
                    .print_parameters()
                    .get_incomprFlowParam(); // be careful with this casting, check the documentation
```
and try to figure out how to modify the the simulation parameters and
run some experiments with different parameters. In particular try the
first two sub-points of the task list point 2.

To get a free-slip channel flow from the current Poiseuille setup, look
inside the definition of `setup_boundary_conditions_2d()`
[setup.h: Lines 39-41](src/setup.h#L39-L41)
that is called by `set_lattice_2d()`
[cbc_example.cpp: Line 67](src/cbc_example_2d.cpp#L67-L67).  
You need to change something, in particular in the next code segment
```
    // Create Velocity boundary conditions everywhere
    boundaryCondition.setVelocityConditionOnBlockBoundaries (
            lattice, Box2D(0, 0, 1, ny-2) );
    boundaryCondition.setVelocityConditionOnBlockBoundaries (
            lattice, Box2D(0, nx-1, 0, 0) );
    boundaryCondition.setVelocityConditionOnBlockBoundaries (
            lattice, Box2D(0, nx-1, ny-1, ny-1) );
```
you need to tell to setVelocityConditionOnBlockBoundaries to use the
free slip condition.

Convert the initialization and the boundary velocities to a uniform profile.
Hint, you need to change `PoiseuilleVelocity` and `PoiseuilleVelocityAndDensity` to
something else
```
    setBoundaryVelocity(lattice, lattice.getBoundingBox(),
                        PoiseuilleVelocity<Real>(parameters));
    setBoundaryDensity(lattice, outlet, ConstantDensity<Real>(1.));
    initializeAtEquilibrium(
        lattice, lattice.getBoundingBox(),
        PoiseuilleVelocityAndDensity<Real, Descriptor>(parameters));
```
Finally put a free slip (specular reflection) on the cylinder, instead of
bounce-back, changing the dynamics. Do it in the following code segment
```
    defineDynamics(lattice, lattice.getBoundingBox(),
                   new CylinderShapeDomain2D<Real, Descriptor>(cx, cy, radius),
                   new plb::BounceBack<Real, Descriptor>);
```

### Run the 3D version
Check now the file `cbc_example_3d.cpp`. First, choose your numerical resolution in:
```
    IncomprFlowParam<Real> parameters =
        lu.initLrefluNodim(000000/*resolution*/, &dimless, 00000 /*u_lb*/,
                           false /*tau, optional*/)
            .printParameters()
            .getIncomprFlowParam();
```
then, try to compile. You will need to modify the CMakeLists.txt
```
### change the following line
set(EXECUTABLE_NAME "cbc_example_2d")
### to
set(EXECUTABLE_NAME "cbc_example_3d")
```
Before running, first save the results of the 2d simulation
```
mv tmp output_2d
```
then try to run the 3d simulation
```
mpirun -np 2 cbc_example
```
#### Drag coefficient
Now it is time to compute the drag coefficient, I can give you three hints:
1. Check the available methods of `OffLatticeBoundaryCondition3D<Real, Descriptor, Array<Real, 3>>`
2. Use the definition of [Drag coefficient](https://en.wikipedia.org/wiki/Drag_coefficient);
3. Use `pcout` to write output file in parallel simulations.

#### Change boundary condition
At point 7 of the function `setupLattice3d` you will find that a function is called to setup the off lattice boundary condition.
Try to figure out how to modify it to use a new boundary condition.

#### Use sponge zones
At point 5. of `setupLattice3d` you will find a call to a function to setup
a sponge zone. Check inside this helper function to understand how it works.
Then, try to setup a sponge zones and to see its effects on the simulation.
For instance, compare the time evolution of the drag coefficient with and without
its implementation.

#### Add a second sphere
Using either Guo (GZS) or Bouzidi (BFL), add a second sphere behind the first one.
Then, try do transform them into ellipsoids.


#### Questions? Read the [FAQ](FAQ.md)

## Bibliography
[1] Z. Guo, C. Zheng, and B. Shi, “An extrapolation method for boundary conditions in lattice Boltzmann method,” Physics of Fluids, vol. 14, no. 6, pp. 2007–2010, Jun. 2002, doi: 10.1063/1.1471914.  
[2] M. Bouzidi, M. Firdaouss, and P. Lallemand, “Momentum transfer of a Boltzmann-lattice fluid with boundaries,” Physics of Fluids, vol. 13, no. 11, pp. 3452–3459, Oct. 2001, doi: 10.1063/1.1399290.  
[3] O. Filippova and D. Hänel, “Grid Refinement for Lattice-BGK Models,” Journal of Computational Physics, vol. 147, no. 1, pp. 219–228, Nov. 1998, doi: 10.1006/jcph.1998.6089.  
[4] R. Mei, L.-S. Luo, and W. Shyy, “An Accurate Curved Boundary Treatment in the Lattice Boltzmann Method,” Journal of Computational Physics, vol. 155, no. 2, pp. 307–330, Nov. 1999, doi: 10.1006/jcph.1999.6334.  
[5] Kruger et al., The lattice Boltzmann method: principles and practice. New York, NY: Springer Berlin Heidelberg, 2016.  
[6] S. Succi, The Lattice Boltzmann equation: for complex states of flowing matter, First edition. Oxford: Oxford University Press, 2018.  
[7] O. P. Malaspinas, “Lattice Boltzmann method for the simulation of viscoelastic fluid flows,” 2009.


