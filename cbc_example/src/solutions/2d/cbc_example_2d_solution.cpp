/* This code is a showcase for the Palabos library.
 *
 * The Palabos software is developed since 2011 by FlowKit-Numeca Group Sarl
 * (Switzerland) and the University of Geneva (Switzerland), which jointly
 * own the IP rights for most of the code base. Since October 2019, the
 * Palabos project is maintained by the University of Geneva and accepts
 * source code contributions from the community.
 *
 * The most recent release of Palabos can be downloaded at
 * <https://palabos.unige.ch/>
 *
 * Contact:
 * Jonas Latt
 * Computer Science Department
 * University of Geneva
 * 7 Route de Drize
 * 1227 Carouge, Switzerland
 * jonas.latt@unige.ch
 *
 * You can redistribute it and/or modify this code
 * under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 */

#include <cmath>
#include <iostream>
#include <tuple>

#include "../../shapes.h"
#include "../../simulationParameters.cpp"  // explicit inclusion because it contains templates
#include "../../simulationParameters.h"
#include "palabos3D.h"
#include "palabos3D.hh"  // explicit inclusion because it contains templates
#include "setupSolution2d.h"

#define DESCRIPTOR D2Q9Descriptor
namespace sp = incompressible_simulation_parameters;
using namespace plb;
using namespace std;
using Real = double;
using Int = plint;

template <typename Real, template <typename U> class Descriptor>
void writeGif(MultiBlockLattice2D<Real, Descriptor> &lattice, plint iter) {
    ImageWriter<Real> imageWriter("leeloo");
    imageWriter.writeScaledGif(createFileName("u", iter, 6),
                               *computeVelocityNorm(lattice));
}

auto set_lattice_2d(IncomprFlowParam<Real> &parameters, bool trt = false) {
    IsoThermalBulkDynamics<Real, DESCRIPTOR> *dynamics = nullptr;
    if (trt) {
        auto dynamic =
            new TruncatedTRTdynamics<Real, DESCRIPTOR>(parameters.getOmega());
        dynamics = dynamic;
    } else {
        auto dynamic = new BGKdynamics<Real, DESCRIPTOR>(parameters.getOmega());
        dynamics = dynamic;
    }
    auto lattice_ptr = new MultiBlockLattice2D<Real, DESCRIPTOR>(
        parameters.getNx(), parameters.getNy(), dynamics);

    OnLatticeBoundaryCondition2D<Real, DESCRIPTOR> *boundaryCondition =
        createLocalBoundaryCondition2D<Real, DESCRIPTOR>();
    setup_boundary_conditions_2d(*lattice_ptr, parameters, *boundaryCondition);
    return tuple{lattice_ptr, boundaryCondition};
}

int main(int argc, char *argv[]) {
    // Palabos initialization
    plbInit(&argc, &argv);
    string outdir = "tmp/";
    if (global::mpi().getRank() == 0) system(("mkdir -p " + outdir).c_str());
    global::directories().setOutputDir(outdir);

    // Define simulation parameters: we use two ad-hoc units helper check
    // simulation_parameters.h for the documentation.
    const Real re = 250;  // NB: Obstacle reynolds!
    sp::Numerics<Real, Int> lu;
    sp::NonDimensional<Real> dimless;
    dimless.initReLxLyLz(re, 24, 4, 4).printParameters();
    IncomprFlowParam<Real> parameters =
        lu.initLrefluNodim(25, &dimless, 0.02, false)
            .printParameters()
            .getIncomprFlowParam();  // be careful with this casting, check the
                                     // documentation

    // Define output constants
    const Real logT = (Real)0.02;
    [[maybe_unused]] const Real imSave = (Real)0.06;
    [[maybe_unused]] const Real vtkSave = (Real)0.02;
    const Real maxT = (Real)2.1;
    writeLogFile(parameters, "Poiseuille flow");

    // Setup the simulation and allocate lattice and boundary conditions
    auto [lattice_ptr, boundaryon] = set_lattice_2d(parameters, true);
    auto &lattice = *lattice_ptr;



    // Main loop over time iterations.
    for (plint iT = 0; iT * parameters.getDeltaT() < maxT; ++iT) {
        // At this point, the state of the lattice corresponds to the
        //   discrete time iT. However, the stored averages
        //   (getStoredAverageEnergy and getStoredAverageDensity) correspond to
        //   the previous time iT-1.

        // For the 2D case we use the the palabos gif writer
        if (iT % parameters.nStep(imSave) == 0) {
            pcout << "Saving Gif ..." << endl;
            writeGif(lattice, iT);
        }

        if (iT % parameters.nStep(logT) == 0) {
            pcout << "step " << iT << "; t=" << iT * parameters.getDeltaT();
        }
        global::timer("iteration").restart();

        // Lattice Boltzmann iteration step.
        lattice.collideAndStream();
        lattice.executeInternalProcessors();  // i.e. boundary conditions in
                                              // this case
        lattice.incrementTime();

        // At this point, the state of the lattice corresponds to the
        //   discrete time iT+1, and the stored averages are upgraded to time
        //   iT.
        if (iT % parameters.nStep(logT) == 0) {
            pcout << "; av energy =" << setprecision(10)
                  << getStoredAverageEnergy<Real>(lattice)
                  << "; av rho =" << getStoredAverageDensity<Real>(lattice)
                  << "; time =" << global::timer("iteration").getTime() << "s"
                  << "; Cd ="  // << cd
                  << std::endl;
            std::string fullName =
                global::directories().getLogOutDir() + "Cd.dat";
            plb_ofstream ofile(fullName.c_str(), std::ostream::app);
        }
        global::timer("iteration").stop();
    }

    if (global::mpi().getRank() == 0)
        system(
            "mkdir -p ./tmp/tmp && gifsicle ./tmp/*.gif > ./tmp/tmp/video.gif "
            "--colors 256 && rm tmp/*.gif "
            " && mv ./tmp/tmp/video.gif tmp && rm -r ./tmp/tmp");


    delete boundaryon;
    delete lattice_ptr;

    return 0;
}
